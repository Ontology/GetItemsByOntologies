﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GetItemsByOntologies;
using OntologyAppDBConnector;

namespace GetItemsByOntologiesTests
{
    [TestClass]
    public class OntologiesTests
    {
        [TestMethod]
        public void GetOntologyOItems()
        {
            var globals = new Globals();
            var ontologyWorker = new OntologyWorker(globals);
            var resultTask = ontologyWorker.GetOntology(new OntologyClasses.BaseClasses.clsOntologyItem
            {
                GUID = "0c5596776b634fc3a9e63858758c8e57",
                Name = "Ontology-Module",
                GUID_Parent = "eb411e2ff93d4a5ebbbac0b5d7ec0197",
                Type = globals.Type_Object
            });

            resultTask.Wait();

            resultTask.Result.Ontology.CreateOntologyLists();
        }

        [TestMethod]
        public void GetOntologyOJoins()
        {
            var globals = new Globals();
            var ontologyWorker = new OntologyWorker(globals);
            var resultTask = ontologyWorker.GetOntology(new OntologyClasses.BaseClasses.clsOntologyItem
            {
                GUID = "466370fdc04d43c7ac697d03d76fc67b",
                Name = "Application-Function-Tree",
                GUID_Parent = "eb411e2ff93d4a5ebbbac0b5d7ec0197",
                Type = globals.Type_Object
            });

            resultTask.Wait();

            resultTask.Result.Ontology.CreateOntologyLists();
        }
    }
}
