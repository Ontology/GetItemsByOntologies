﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;

namespace GetItemsByOntologies
{
    public class OntologyWorker : NotifyPropertyChange
    {

        private object workerLocker = new object();
        private ResultOntology resultOntology;

        public ResultOntology ResultOntology
        {
            get
            {
                lock (workerLocker)
                {
                    return resultOntology;
                }
                
            }
            set
            {
                lock (workerLocker)
                {
                    resultOntology = value;
                }
                
                RaisePropertyChanged(nameof(ResultOntology));
            }
        }

        private ResultORelItems resultOntologyItems;
        public ResultORelItems ResultOntologyItems
        {
            get
            {
                lock (workerLocker)
                {
                    return resultOntologyItems;
                }
            }
            set
            {
                lock (workerLocker)
                {
                    resultOntologyItems = value;
                }
                RaisePropertyChanged(nameof(ResultOntologyItems));
            }
        }

        public bool SearchObjectRelations { get; set; }
        public bool SearchObjectAttributes { get; set; }

        private Globals globals;

        public async Task<ResultORelItems> GetClassRelations(clsOntologyItem classItem)
        {
            var result = new ResultORelItems
            {
                Result = globals.LState_Success.Clone(),
                OntologyLists = new OntologyLists()
            };

            var searchClassAtts = new List<clsClassAtt>
            {
                new clsClassAtt
                {
                    ID_Class = classItem.GUID
                }
            };

            var dbReaderClassAtt = new OntologyModDBConnector(globals);
            result.Result = dbReaderClassAtt.GetDataClassAtts(searchClassAtts);

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            var classes = new List<clsOntologyItem>
            {
                classItem
            };

            classes.AddRange(dbReaderClassAtt.Classes1);

            result.OntologyLists.AttributeTypes =  dbReaderClassAtt.ClassAtts.GroupBy(clsAtt => new { GUID = clsAtt.ID_AttributeType, Name = clsAtt.Name_AttributeType, GUID_Parent = clsAtt.ID_DataType }).
                Select(clsAtt => new clsOntologyItem { GUID = clsAtt.Key.GUID, Name = clsAtt.Key.Name, GUID_Parent = clsAtt.Key.GUID_Parent }).ToList();
            result.OntologyLists.ClassAttributes = dbReaderClassAtt.ClassAtts;

            var searchClassRel = new List<clsClassRel>
            {
                new clsClassRel
                {
                    ID_Class_Left = classItem.GUID
                }
            };

            var dbReaderClassRelLeftRight = new OntologyModDBConnector(globals);
            result.Result = dbReaderClassRelLeftRight.GetDataClassRel(searchClassRel);

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            classes.AddRange(dbReaderClassRelLeftRight.ClassRels.Select(clsRel => new clsOntologyItem
            {
                GUID = clsRel.ID_Class_Right,
                Name = clsRel.Name_Class_Right,
                Type = globals.Type_Class
            }));

            result.OntologyLists.ClassRelations = dbReaderClassRelLeftRight.ClassRels;

            searchClassRel = new List<clsClassRel>
            {
                new clsClassRel
                {
                    ID_Class_Right = classItem.GUID
                }
            };

            var dbReaderClassRelRightLeft = new OntologyModDBConnector(globals);
            result.Result = dbReaderClassRelRightLeft.GetDataClassRel(searchClassRel);

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            classes.AddRange(dbReaderClassRelRightLeft.ClassRels.Select(clsRel => new clsOntologyItem
            {
                GUID = clsRel.ID_Class_Left,
                Name = clsRel.Name_Class_Left,
                Type = globals.Type_Class
            }));
            result.OntologyLists.ClassRelations.AddRange(dbReaderClassRelRightLeft.ClassRels);

            result.OntologyLists.Classes = classes.GroupBy(cls => new { GUID = cls.GUID, Name = cls.Name }).Select(cls => new clsOntologyItem
            {
                GUID = cls.Key.GUID,
                Name = cls.Key.Name,
                Type = globals.Type_Class
            }).ToList();

            return result;
        }

        public async Task<ResultORelItems> GetObjectRelations(clsOntologyItem objectItem)
        {
            var result = new ResultORelItems
            {
                Result = globals.LState_Success.Clone(),
                OntologyLists = new OntologyLists()
            };


            var searchAttributes = new List<clsObjectAtt> { new clsObjectAtt
                {
                    ID_Object = objectItem.GUID
                }
            };

            var dbReaderObjAtt = new OntologyModDBConnector(globals);
            result.Result = dbReaderObjAtt.GetDataObjectAtt(searchAttributes);

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.OntologyLists.ObjectAttributes = dbReaderObjAtt.ObjAtts;
            result.OntologyLists.AttributeTypes = dbReaderObjAtt.AttributeTypes;

            var searchLeftRightRels = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = objectItem.GUID
                }
            };

            var dbReaderObjRelLeftRight = new OntologyModDBConnector(globals);

            result.Result = dbReaderObjRelLeftRight.GetDataObjectRel(searchLeftRightRels);
            var objects = dbReaderObjRelLeftRight.ObjectRels.Where(objRel => objRel.Ontology == globals.Type_Object).Select(objRel =>
                new clsOntologyItem
                {
                    GUID = objRel.ID_Other,
                    Name = objRel.Name_Other,
                    GUID_Parent = objRel.ID_Parent_Other,
                    Type = objRel.Ontology
                }).ToList();

            var attributeTypes = dbReaderObjRelLeftRight.ObjectRels.Where(objRel => objRel.Ontology == globals.Type_AttributeType).Select(objRel =>
                new clsOntologyItem
                {
                    GUID = objRel.ID_Other,
                    Name = objRel.Name_Other,
                    GUID_Parent = objRel.ID_Parent_Other,
                    Type = objRel.Ontology
                }).ToList();

            var relationTypes = dbReaderObjRelLeftRight.ObjectRels.Where(objRel => objRel.Ontology == globals.Type_RelationType).Select(objRel =>
                new clsOntologyItem
                {
                    GUID = objRel.ID_Other,
                    Name = objRel.Name_Other,
                    Type = objRel.Ontology
                }).ToList();

            var classes = dbReaderObjRelLeftRight.ObjectRels.Where(objRel => objRel.Ontology == globals.Type_Class).Select(objRel =>
                new clsOntologyItem
                {
                    GUID = objRel.ID_Other,
                    Name = objRel.Name_Other,
                    GUID_Parent = objRel.ID_Parent_Other,
                    Type = objRel.Ontology
                }).ToList();

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.OntologyLists.ObjectRelations = dbReaderObjRelLeftRight.ObjectRels;
            objects.Add(objectItem);
            objects.AddRange(dbReaderObjRelLeftRight.ObjectRels.Select(objRel =>
                new clsOntologyItem
                {
                    GUID = objRel.ID_Object,
                    Name = objRel.Name_Object,
                    GUID_Parent = objRel.ID_Parent_Object,
                    Type = globals.Type_Object
                }));

            


            var searchRightLeftRels = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Other = objectItem.GUID
                }
            };

            var dbReaderObjRelRightLeft = new OntologyModDBConnector(globals);

            result.Result = dbReaderObjRelRightLeft.GetDataObjectRel(searchRightLeftRels);

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.OntologyLists.ObjectRelations.AddRange(dbReaderObjRelRightLeft.ObjectRels);
            result.OntologyLists.Objects = objects.GroupBy(objItm => new { GUID = objItm.GUID, Name = objItm.Name, GUID_Parent = objItm.GUID_Parent, Type = objItm.Type }).Select(objItm => new clsOntologyItem
            {
                GUID = objItm.Key.GUID,
                Name = objItm.Key.Name,
                GUID_Parent = objItm.Key.GUID_Parent,
                Type = objItm.Key.Type
            }).ToList();

            result.OntologyLists.Classes = classes.GroupBy(objItm => new { GUID = objItm.GUID, Name = objItm.Name, GUID_Parent = objItm.GUID_Parent, Type = objItm.Type }).Select(objItm => new clsOntologyItem
            {
                GUID = objItm.Key.GUID,
                Name = objItm.Key.Name,
                GUID_Parent = objItm.Key.GUID_Parent,
                Type = objItm.Key.Type
            }).ToList();

            result.OntologyLists.AttributeTypes = attributeTypes.GroupBy(objItm => new { GUID = objItm.GUID, Name = objItm.Name, GUID_Parent = objItm.GUID_Parent, Type = objItm.Type }).Select(objItm => new clsOntologyItem
            {
                GUID = objItm.Key.GUID,
                Name = objItm.Key.Name,
                GUID_Parent = objItm.Key.GUID_Parent,
                Type = objItm.Key.Type
            }).ToList();

            result.OntologyLists.RelationTypes = relationTypes.GroupBy(objItm => new { GUID = objItm.GUID, Name = objItm.Name, Type = objItm.Type }).Select(objItm => new clsOntologyItem
            {
                GUID = objItm.Key.GUID,
                Name = objItm.Key.Name,
                Type = objItm.Key.Type
            }).ToList();

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.OntologyLists.ObjectRelations.AddRange(dbReaderObjRelRightLeft.ObjectRels);

            return result;
        }

        public async Task<ResultOntology> GetOntology(clsOntologyItem ontologyItem)
        {
            var result = new ResultOntology()
            {
                Result = globals.LState_Success.Clone(),
                Ontology = new Ontology(globals, SearchObjectAttributes, SearchObjectRelations)
            };

            result.Ontology.OntologyItem = ontologyItem;

            var resultOItems = GetOItemsOfOntology(ontologyItem);

            if (resultOItems.Result.GUID == globals.LState_Error.GUID)
            {
                result.Result = resultOItems.Result;
                ResultOntology = result;
                return result;
            }

            var resultOJoins = GetOntologyJoinsOfOntology(ontologyItem);

            if (resultOJoins.Result.GUID == globals.LState_Error.GUID)
            {
                result.Result = resultOItems.Result;
                ResultOntology = result;
                return result;
            }

            result.Ontology.OItemsOfOntologies = resultOItems.OItemsOfOntologies;
            result.Ontology.OItemsOfJoin = resultOJoins.OntologyJoins;
           
            return result;
        }

        private ResultOItems GetOItemsOfOntology(clsOntologyItem ontologyItem)
        {
            var dbReaderOItems = new OntologyModDBConnector(globals);

            var result = new ResultOItems
            {
                Result = globals.LState_Success.Clone()
            };

            var searchOItems = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = ontologyItem.GUID,
                    ID_RelationType = globals.RelationType_contains.GUID,
                    ID_Parent_Other = globals.Class_OntologyItems.GUID
                }
            };
            
            result.Result = dbReaderOItems.GetDataObjectRel(searchOItems);

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.OItemsOfOntologies = new OItemsOfOntologies();
            var dbReaderRef = new OntologyModDBConnector(globals);

            var searchRefItems =
                dbReaderOItems.ObjectRels.Select(oItem => new clsObjectRel { ID_Object = oItem.ID_Other }).ToList();

            if (searchRefItems.Any())
            {
                result.Result = dbReaderRef.GetDataObjectRel(searchRefItems);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }
            

            result.OItemsOfOntologies.ReferenceItems =
                dbReaderRef.ObjectRels.Where(
                    oItemRef => oItemRef.ID_RelationType == globals.RelationType_belongingAttribute.GUID ||
                                oItemRef.ID_RelationType == globals.RelationType_belongingObject.GUID ||
                                oItemRef.ID_RelationType == globals.RelationType_belongingClass.GUID ||
                                oItemRef.ID_RelationType == globals.RelationType_belongingRelationType.GUID).ToList();
            result.OItemsOfOntologies.RelationRules =
                dbReaderRef.ObjectRels.Where(oItemRef =>
                    oItemRef.ID_RelationType == globals.RelationType_contains.GUID &&
                    oItemRef.ID_Parent_Other == globals.Class_OntologyRelationRule.GUID).ToList();

            return result;

        }

        private ResultOJoins GetOntologyJoinsOfOntology(clsOntologyItem ontologyItem)
        {
            var result = new ResultOJoins
            {
                Result = globals.LState_Success.Clone()
            };

            var dbReaderOJoins = new OntologyModDBConnector(globals);

            var searchOJoins = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = ontologyItem.GUID,
                    ID_RelationType =  globals.RelationType_contains.GUID,
                    ID_Parent_Other = globals.Class_OntologyJoin.GUID
                }
            };

            result.Result = dbReaderOJoins.GetDataObjectRel(searchOJoins);

            if (result.Result.GUID == globals.LState_Error.GUID)
            {
                return result;
            }

            result.OntologyJoins = new List<OItemsOfOJoin>();

            var searchOItems = dbReaderOJoins.ObjectRels.Select(oJoin => new clsObjectRel
            {
                ID_Object = oJoin.ID_Other,
                ID_RelationType = globals.RelationType_contains.GUID,
                ID_Parent_Other = globals.Class_OntologyItems.GUID
            }).ToList();

            var dbReaderOItems = new OntologyModDBConnector(globals);

            if (searchOItems.Any())
            {
                result.Result = dbReaderOItems.GetDataObjectRel(searchOItems);

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

            }

            var searchRefItems = dbReaderOItems.ObjectRels.Select(oItem => new clsObjectRel
            {
                ID_Object =  oItem.ID_Other
            }).ToList();

            var dbReaderRefItems = new OntologyModDBConnector(globals);

            if (searchRefItems.Any())
            {
                result.Result = dbReaderRefItems.GetDataObjectRel(searchRefItems);
            

                if (result.Result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }
            }

            foreach (var join in dbReaderOJoins.ObjectRels)
            {
                var oItems = dbReaderOItems.ObjectRels.Where(oJoin => oJoin.ID_Object == join.ID_Other).ToList();
                
                var refItems =
                    dbReaderRefItems.ObjectRels.Where(
                        oItemRef => oItemRef.ID_RelationType == globals.RelationType_belongingAttribute.GUID ||
                                    oItemRef.ID_RelationType == globals.RelationType_belongingObject.GUID ||
                                    oItemRef.ID_RelationType == globals.RelationType_belongingClass.GUID ||
                                    oItemRef.ID_RelationType == globals.RelationType_belongingRelationType.GUID).ToList();
                var ruleItems =
                    dbReaderRefItems.ObjectRels.Where(oItemRef =>
                        oItemRef.ID_RelationType == globals.RelationType_contains.GUID &&
                        oItemRef.ID_Parent_Other == globals.Class_OntologyRelationRule.GUID).ToList();

                var oJOin = new OItemsOfOJoin
                {
                    OntologyJoin = join,
                    OItemsOfOntologyJoin = oItems,
                    ReferenceItems = refItems,
                    RuleItems = ruleItems
                    
                };

                result.OntologyJoins.Add(oJOin);
            }

            return result;
        }

        public OntologyWorker(Globals globals)
        {
            this.globals = globals;
        }
    }


    public class ResultOItems
    {
        public clsOntologyItem Result { get; set; }
        public OItemsOfOntologies OItemsOfOntologies { get; set; }
    }

    public class ResultOJoins
    {
        public clsOntologyItem Result { get; set; }
        public List<OItemsOfOJoin> OntologyJoins { get; set; }
    }

    public class OItemsOfOJoin
    {
        public clsObjectRel OntologyJoin { get; set; }
        public List<clsObjectRel> OItemsOfOntologyJoin { get; set; }
        public List<clsObjectRel> ReferenceItems { get; set; }
        public List<clsObjectRel> RuleItems { get; set; }
    }

    public class OItemsOfOntologies
    {
        public List<clsObjectRel> OItemsOfOntology { get; set; }
        public List<clsObjectRel> ReferenceItems { get; set; }
        public List<clsObjectRel> RelationRules { get; set; }
    }

    public class ResultOntology
    {
        public clsOntologyItem Result { get; set; }
        public Ontology Ontology { get; set; }
    }

    public class ResultORelItems
    {
        public clsOntologyItem Result { get; set; }
        public OntologyLists OntologyLists { get; set; }
    }

    public class OntologyLists
    {
        public List<clsOntologyItem> Classes { get; set; }
        public List<clsOntologyItem> Objects { get; set; }

        public List<clsOntologyItem> AttributeTypes { get; set; }

        public List<clsOntologyItem> RelationTypes { get; set; }

        public List<clsClassAtt> ClassAttributes { get; set; }

        public List<clsClassRel> ClassRelations { get; set; }

        public List<clsObjectAtt> ObjectAttributes { get; set; }

        public List<clsObjectRel> ObjectRelations { get; set; }
    }

    public class Ontology
    {
        private Globals globals;
        public clsOntologyItem OntologyItem { get; set; }

        public List<OItemsOfOJoin> OItemsOfJoin { get; set; }

        public OItemsOfOntologies OItemsOfOntologies { get; set; }

        public OntologyLists OntologyLists { get; set; }

        public bool SearchObjectAttributes { get; set; }

        public bool SearchObjectRelations { get; set; }

        public Ontology(Globals globals, bool searchObjectAttributes, bool searchObjectRelations)
        {
            this.globals = globals;

            SearchObjectAttributes = searchObjectAttributes;
            SearchObjectRelations = searchObjectRelations;
        }

        public clsOntologyItem CreateOntologyLists()
        {
            var result = globals.LState_Success.Clone();

            var classesRel = OItemsOfOntologies.ReferenceItems.Where(refItem => refItem.Ontology == globals.Type_Class).ToList();
            classesRel.AddRange(OItemsOfJoin.SelectMany(oJoin => oJoin.ReferenceItems.Where(refItem => refItem.Ontology == globals.Type_Class)));

            var refItemsOfJoins = OItemsOfJoin.SelectMany(oJoin => oJoin.ReferenceItems);

            var attributeTypesRel =
                OItemsOfOntologies.ReferenceItems.Where(refItem => refItem.Ontology == globals.Type_AttributeType).ToList();
            attributeTypesRel.AddRange(refItemsOfJoins.Where(refItem => refItem.Ontology == globals.Type_AttributeType));

            var relationTypesRel =
                OItemsOfOntologies.ReferenceItems.Where(refItem => refItem.Ontology == globals.Type_RelationType).ToList();
            relationTypesRel.AddRange(refItemsOfJoins.Where(refItem => refItem.Ontology == globals.Type_RelationType));

            var objectsRel =
                OItemsOfOntologies.ReferenceItems.Where(refItem => refItem.Ontology == globals.Type_Object).ToList();
            objectsRel.AddRange(refItemsOfJoins.Where(refItem => refItem.Ontology == globals.Type_Object));

            var classes =
                classesRel.GroupBy(
                    cls =>
                        new clsOntologyItem
                        {
                            GUID = cls.ID_Other,
                            Name = cls.Name_Other,
                            GUID_Parent = cls.ID_Parent_Other,
                            Type = cls.Ontology
                        })
                    .Select(
                        cls =>
                            new clsOntologyItem
                            {
                                GUID = cls.Key.GUID,
                                Name = cls.Key.Name,
                                GUID_Parent = cls.Key.GUID_Parent,
                                Type = cls.Key.Type
                            }).ToList();

            var relationTypes = relationTypesRel.GroupBy(
                    cls =>
                        new clsOntologyItem
                        {
                            GUID = cls.ID_Other,
                            Name = cls.Name_Other,
                            Type = cls.Ontology
                        })
                    .Select(
                        cls =>
                            new clsOntologyItem
                            {
                                GUID = cls.Key.GUID,
                                Name = cls.Key.Name,
                                Type = cls.Key.Type
                            }).ToList();

            var attributeTypes = attributeTypesRel.GroupBy(
                    cls =>
                        new clsOntologyItem
                        {
                            GUID = cls.ID_Other,
                            Name = cls.Name_Other,
                            GUID_Parent = cls.ID_Parent_Other,
                            Type = cls.Ontology
                        })
                    .Select(
                        cls =>
                            new clsOntologyItem
                            {
                                GUID = cls.Key.GUID,
                                Name = cls.Key.Name,
                                GUID_Parent = cls.Key.GUID_Parent,
                                Type = cls.Key.Type
                            }).ToList();

            var objects = objectsRel.GroupBy(
                    cls =>
                        new clsOntologyItem
                        {
                            GUID = cls.ID_Other,
                            Name = cls.Name_Other,
                            GUID_Parent = cls.ID_Parent_Other,
                            Type = cls.Ontology
                        })
                    .Select(
                        cls =>
                            new clsOntologyItem
                            {
                                GUID = cls.Key.GUID,
                                Name = cls.Key.Name,
                                GUID_Parent = cls.Key.GUID_Parent,
                                Type = cls.Key.Type
                            }).ToList();

            var ruleItems = OItemsOfJoin.SelectMany(oJoin => oJoin.RuleItems);

            var searchObjects = (from rule in OItemsOfOntologies.RelationRules
                                     join refItem in OItemsOfOntologies.ReferenceItems.Where(refItm => refItm.Ontology == globals.Type_Class) on rule.ID_Object equals refItem.ID_Object
                                     select new clsOntologyItem { GUID_Parent = refItem.ID_Other }).ToList();

            searchObjects.AddRange(from rule in ruleItems
                                   join refItem in refItemsOfJoins.Where(refItm => refItm.Ontology == globals.Type_Class) on rule.ID_Object equals refItem.ID_Object
                                   select new clsOntologyItem { GUID_Parent = refItem.ID_Other });

            if (searchObjects.Any())
            {
                var dbReaderObjects = new OntologyModDBConnector(globals);

                result = dbReaderObjects.GetDataObjects(searchObjects);

                if (result.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                objects.AddRange(from objectItem in dbReaderObjects.Objects1
                                 join existingObject in objects on objectItem.GUID equals existingObject.GUID into existingObjects
                                 from existingObject in existingObjects.DefaultIfEmpty()
                                 where existingObject == null
                                 select objectItem);
            }
            
            
            OntologyLists = new OntologyLists
            {
                AttributeTypes = attributeTypes,
                Classes = classes,
                RelationTypes = relationTypes,
                Objects = objects
            };

            
            var searchClassAtt = (from cls in classes
                                  from att in attributeTypes
                                  select new { ClassItem = cls, AttItem = att }).GroupBy(classAtt => new { ID_Class = classAtt.ClassItem.GUID, ID_Attribute = classAtt.AttItem.GUID })
                                  .Select(classAtt => new clsClassAtt { ID_AttributeType = classAtt.Key.ID_Attribute, ID_Class = classAtt.Key.ID_Class }).ToList();

            if (searchClassAtt.Any())
            {
                var dbReaderClassAtt = new OntologyModDBConnector(globals);
                result = dbReaderClassAtt.GetDataClassAtts(searchClassAtt);

                OntologyLists.ClassAttributes = dbReaderClassAtt.ClassAtts;
            }

            var searchClassRel = (from clsLeft in classes
                                  from clsRight in classes
                                  select new { ClassLeft = clsLeft, ClassRight = clsRight }).GroupBy(classAtt => new { ID_Class_Left = classAtt.ClassLeft.GUID, ID_Class_Right = classAtt.ClassRight.GUID })
                                  .Select(classAtt => new clsClassRel { ID_Class_Left = classAtt.Key.ID_Class_Left, ID_Class_Right = classAtt.Key.ID_Class_Right }).ToList();

            if (searchClassRel.Any())
            {
                var dbReaderClassRel = new OntologyModDBConnector(globals);
                result = dbReaderClassRel.GetDataClassRel(searchClassRel);

                OntologyLists.ClassRelations = dbReaderClassRel.ClassRels;
            }

            var searchPreObjectAttributes = (from cls in classes
                                             from att in attributeTypes
                                             select new clsObjectAtt
                                             {
                                                 ID_Class = cls.GUID,
                                                 ID_AttributeType = att.GUID
                                             }).GroupBy(clsAtt => new { ID_Class = clsAtt.ID_Class, ID_AttributeType = clsAtt.ID_AttributeType });


            if (SearchObjectAttributes)
            {
                var searchObjectAttributes = searchPreObjectAttributes.Select(clsAtt => new clsObjectAtt { ID_Class = clsAtt.Key.ID_Class, ID_AttributeType = clsAtt.Key.ID_AttributeType }).ToList();


                if (searchObjectAttributes.Any())
                {
                    var dbReaderObjAtt = new OntologyModDBConnector(globals);
                    result = dbReaderObjAtt.GetDataObjectAtt(searchObjectAttributes);

                    OntologyLists.ObjectAttributes = (from obj in objects
                                                      join objAtt in dbReaderObjAtt.ObjAtts on obj.GUID equals objAtt.ID_Object
                                                      select objAtt).ToList();
                }
            }
            
            if (SearchObjectRelations)
            {
                var searchObjectRelations = (from clsLeft in classes
                                             from clsRight in classes
                                             select new clsObjectRel
                                             {
                                                 ID_Parent_Object = clsLeft.GUID,
                                                 ID_Parent_Other = clsRight.GUID
                                             }).GroupBy(clsAtt => new { ID_ParentObject = clsAtt.ID_Parent_Object, ID_ParentOther = clsAtt.ID_Parent_Other })
                                         .Select(clsAtt => new clsObjectRel { ID_Parent_Object = clsAtt.Key.ID_ParentObject, ID_Parent_Other = clsAtt.Key.ID_ParentOther }).ToList();

                if (searchObjectRelations.Any())
                {
                    var dbReaderObjRel = new OntologyModDBConnector(globals);
                    result = dbReaderObjRel.GetDataObjectRel(searchObjectRelations);

                    OntologyLists.ObjectRelations = (from obj in objects
                                                     join objRelLeft in dbReaderObjRel.ObjectRels on obj.GUID equals objRelLeft.ID_Object
                                                     join objRelRight in dbReaderObjRel.ObjectRels on obj.GUID equals objRelRight.ID_Other
                                                     select objRelLeft).ToList();
                }
            }
            
            return result;
        }
    }
}
